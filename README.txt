Video Popup

Video popup module will provide you the 'Video Popup' field formatter for the field type 'image'.
This use the default Drupal 8 modal popup. Without using any external libraries, it gives you simple field formatter.
